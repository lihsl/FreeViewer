OS = $(shell uname)
ifeq ($(OS), Linux) 
TARGETS += x11vnc libvncserver
else ifeq ($(OS), Darwin)
TARGETS += x11vnc
else ifeq ($(OS), MinGW)
endif

CLEANS = $(TARGETS:%=clean-%)

all: $(TARGETS)
	@echo complated.
x11vnc: x11vnc/src/x11vnc
	cp x11vnc/src/x11vnc build/
x11vnc/src/x11vnc: x11vnc/Makefile
	cd x11vnc; make
x11vnc/Makefile:
	cd x11vnc; ./configure --without-ssl --without-crypto --without-fbdev CFLAGS="-w -O1";
clean-x11vnc:
	cd x11vnc; make clean; rm Makefile


libvncserver: libvncserver/libvncserver.so
	cd libvncserver; cp -L libvncserver.so* libvncclient.so* ../build
libvncserver/libvncserver.so: libvncserver/Makefile
	cd libvncserver; make
libvncserver/Makefile:
	cd libvncserver; cmake .
clean-libvncserver:
	cd libvncserver; make clean; rm Makefile CMakeCache.txt


clean: $(CLEANS)
	rm -rf build/*
